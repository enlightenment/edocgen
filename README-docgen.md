# edocgen

This is the EFL documentation generator. It takes documentation described in
Eo files and turns it into a DokuWiki structure (with possibility of adapting
it to other systems later on).

To generate documentation, you need a `dokuwiki` directory in the current
path (see `--help` for `gendoc.lua` for how to alter the path). Then run
the `gendoc.lua` script with `elua`, passing a path to eo files to it.
If you don't pass a path, system eo files are used.

Generally it is recommended that you pass it `src/lib` from your EFL tree
and symlink your DokuWiki installation in the current path.