local eolian = require("eolian")
local util = require("docgen.util")
local eoutils = require("docgen.eolian_utils")
local writer = require("docgen.writer")

local M = {}

local has_notes = false
local has_title = false
local root_nspace

local add_since = function(str, since)
    if not since then
        return str
    end
    local ret = writer.Buffer():write_i("Since " .. since):finish()
    if not str then
        return ret
    end
    return str .. "\n\n" .. ret
end

M.brief_str_get = function(eos, obj, obj2)
    if not obj and not obj2 then
        return nil
    end
    if not obj then
        obj = obj2
    end
    return writer.Buffer():write_doc_string(obj:summary_get(), eos):finish()
end

M.full_str_get = function(eos, obj, obj2, write_since)
    if not obj and not obj2 then
        return "No description supplied."
    end
    if not obj then
        obj, obj2 = obj2, obj
    end
    local sum1 = obj:summary_get() or "No description supplied."
    local desc1 = obj:description_get()
    local edoc = ""
    local since
    if obj2 then
        local sum2 = obj2:summary_get()
        local desc2 = obj2:descirption_get()
        if not desc2 then
            if sum2 then
                edoc = "\n\n" .. sum2
            end
        else
            edoc = "\n\n" .. sum2 .. "\n\n" .. desc2
        end
        if write_since then
            since = obj2:since_get()
        end
    end
    if not since and write_since then
        since = obj:since_get()
    end
    if not desc1 then
        return add_since(writer.Buffer():write_doc_string(
            sum1 .. edoc, eos):finish(), since)
    end
    return add_since(writer.Buffer():write_doc_string(
        sum1 .. "\n\n" .. desc1 .. edoc, eos):finish(), since)
end

M.title_str_get = function(str)
    if has_title then
        return "~~Title: " .. str .. "~~"
    end
    return str
end

M.editable_get = function(ns, name)
    local buf = writer.Buffer()
    buf:write_editable(ns, name)
    return buf:finish():sub(1, #buf - 2)
end

M.link_target_get = function(target)
    if type(target) == "table" then
        if target[#target] == false then
            target[#target] = nil
            target = ":" .. root_nspace .. "-include:"
                         .. table.concat(target, ":")
        else
            target[#target] = nil
            target = ":" .. root_nspace .. ":"
                         .. table.concat(target, ":")
        end
    end
    return target
end

local find_parent_briefdoc
find_parent_briefdoc = function(eos, fulln, cl)
    local pimpl, pcl = eoutils.parent_impl_get(fulln, cl)
    if not pimpl then
        return M.brief_str_get(eos, nil)
    end
    local pdoc = pimpl:documentation_get(eolian.function_type.METHOD)
    local pdocf = eoutils.impl_fallback_doc_get(pimpl)
    if not pdoc and not pdocf then
        return find_parent_briefdoc(eos, fulln, pcl)
    end
    return M.brief_str_get(eos, pdocf)
end

M.impl_description_get = function(eos, impl, cl)
    local over = eoutils.impl_is_overridden(impl, cl)
    local bdoc

    local doc = impl:documentation_get(eolian.function_type.METHOD)
    local docf = eoutils.impl_fallback_doc_get(impl)
    if over and not doc and not docf then
        bdoc = find_parent_briefdoc(eos, impl:name_get(), cl)
    else
        bdoc = M.brief_str_get(eos, docf)
    end
    return bdoc
end

local nspaces_group = function(ns)
    if #ns <= 2 then
        return ns[1]
    end

    if ns[1] == "efl" and (ns[2] == "class" or ns[2] == "interface" or
                           ns[2] == "object" or ns[2] == "promise") then
        return ns[1]
    end

    return ns[1] .. "." .. ns[2]
end

local nspaces_filter = function(items, ns)
    local out = {}

    for _, item in ipairs(items) do
        local group = nspaces_group(eoutils.obj_nspaces_get(item))
        if group == ns then out[#out + 1] = item end
    end

    return out
end

M.ref_groups_get = function(eos)
    local classlist = util.filter_prop_not(eos:classes_get(), "is_beta")
    local classlist = util.filter_prop_not(eos:classes_get(), "is_beta")
    local aliases = util.filter_prop_not(eos:aliases_get(), "is_beta")
    local structs = util.filter_prop_not(eos:structs_get(), "is_beta")
    local enums = util.filter_prop_not(eos:enums_get(), "is_beta")
    local consts = util.filter_prop_not(eos:constants_get(), "is_beta")

    local grouped = {}
    local groups = {}
    for i, cl in ipairs(classlist) do
        local ns = eoutils.obj_nspaces_get(cl)
        local name = nspaces_group(eoutils.obj_nspaces_get(cl))

        local group = grouped[name]
        if not group then
            group = {}
            grouped[name] = group
            groups[#groups + 1] = name
        end

        group[#group + 1] = cl
    end
    table.sort(groups)

    for i = 1, #groups do
        local ns = groups[i]
        local classes = {}
        local ifaces = {}
        local mixins = {}

        for i, cl in ipairs(grouped[ns]) do
            local tp = cl:type_get()
            if tp == eolian.class_type.REGULAR or tp == eolian.class_type.ABSTRACT then
                classes[#classes + 1] = cl
            elseif tp == eolian.class_type.MIXIN then
                mixins[#mixins + 1] = cl
            elseif tp == eolian.class_type.INTERFACE then
                ifaces[#ifaces + 1] = cl
            end
        end
        groups[i] = {
            ns, classes, ifaces, mixins,
            nspaces_filter(aliases, ns),
            nspaces_filter(structs, ns),
            nspaces_filter(enums, ns),
            nspaces_filter(consts, ns)
        }
    end

    return groups
end

M.init = function(root, use_notes, use_title)
    root_nspace = root
    has_notes = use_notes
    has_title = use_title
end

return M
