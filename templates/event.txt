{*title*}

===== Description =====

{*doc.full_str_get(eos, ev_obj:documentation_get(), nil, true)*}

{*doc.editable_get(page_ns, "description")*}

===== Signature =====

<code>
{*ev_m.signature_get(ev_obj)*}
</code>

===== C information =====

<code c>
{*ev_m.c_signature_get(ev_obj)*}
</code>

===== C usage =====

<code c>
{*ev_m.example_get(ev_obj)*}
</code>
