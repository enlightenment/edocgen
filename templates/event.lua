local eolian = require("eolian")
local eoutils = require("docgen.eolian_utils")

local M = {}

M.example_get = function(ev)
    local evcn = ev:c_macro_get()
    local evcnl = evcn:lower()

    local dtype = "Data *"

    local tbl = { "static void\n" }
    tbl[#tbl + 1] = "on_"
    tbl[#tbl + 1] = evcnl
    tbl[#tbl + 1] = "(void *data, const Efl_Event *event)\n{\n    "
    tbl[#tbl + 1] = eoutils.type_cstr_get(ev:type_get(), "info = event->info;\n")
    tbl[#tbl + 1] = "    Eo *obj = event->object;\n    "
    tbl[#tbl + 1] = eoutils.type_cstr_get(dtype, "d = data;\n\n")
    tbl[#tbl + 1] = "    /* event hander code */\n}\n\n"
    tbl[#tbl + 1] = "static void\nsetup_event_handler(Eo *obj, "
    tbl[#tbl + 1] = eoutils.type_cstr_get(dtype, "d")
    tbl[#tbl + 1] = ")\n{\n"
    tbl[#tbl + 1] = "    efl_event_callback_add(obj, "
    tbl[#tbl + 1] = evcn
    tbl[#tbl + 1] = ", on_"
    tbl[#tbl + 1] = evcnl
    tbl[#tbl + 1] = ", d);\n}\n"

    return table.concat(tbl)
end

M.signature_get = function(ev)
    local buf = { ev:short_name_get() }

    if ev:scope_get() == eolian.object_scope.PRIVATE then
        buf[#buf + 1] = " @private"
    elseif ev:scope_get() == eolian.object_scope.PROTECTED then
        buf[#buf + 1] = " @protected"
    end

    if ev:is_beta() then
        buf[#buf + 1] = " @beta"
    end
    if ev:is_hot() then
        buf[#buf + 1] = " @hot"
    end
    if ev:is_restart() then
        buf[#buf + 1] = " @restart"
    end

    local etp = ev:type_get()
    if etp then
        buf[#buf + 1] = ": "
        buf[#buf + 1] = eoutils.obj_serialize(etp)
    end

    buf[#buf + 1] = ";"

    return table.concat(buf)
end

M.c_signature_get = function(ev)
    local csbuf = { ev:c_macro_get(), "(" }

    csbuf[#csbuf + 1] = eoutils.type_cstr_get(ev:type_get())

    if ev:is_beta() then
        csbuf[#csbuf + 1] = ", @beta"
    end
    if ev:is_hot() then
        csbuf[#csbuf + 1] = ", @hot"
    end
    if ev:is_restart() then
        csbuf[#csbuf + 1] = ", @restart"
    end

    csbuf[#csbuf + 1] = ")";

    return table.concat(csbuf)
end

return M
