{*title*}

===== Description =====

{*doc.full_str_get(eos, type_obj:documentation_get(), nil, true)*}

{*doc.editable_get(page_ns, "description")*}

===== Signature =====

<code>
{*eoutils.obj_serialize(type_obj)*}
</code>

===== C signature =====

<code c>
{*eoutils.obj_serialize_c(type_obj, page_ns)*}
</code>
